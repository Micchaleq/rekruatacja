<?php

namespace Polcode\Bundle\RecruitmentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ApiController extends Controller
{
    public function indexAction(){
        
        $em = $this->getDoctrine()->getManager();
        $workers = $em->getRepository('PolcodeRecruitmentBundle:Employee')->findAll();
        $serializer = $this->get('jms_serializer');
        
        return new \Symfony\Component\HttpFoundation\Response($serializer->serialize($workers, 'json'));
    }
    public function managerAction($id){
        
        $em = $this->getDoctrine()->getManager();
        $manager = $em->getRepository('PolcodeRecruitmentBundle:AM')->findOneById($id);
        if(!$manager)
        {
            throw $this->createNotFoundException('Brak Manager-a');
        }
        $workers = $manager->getEmployees();        
        $serializer = $this->get('jms_serializer');
        
        return new \Symfony\Component\HttpFoundation\Response($serializer->serialize($workers, 'json'));
    }

}
