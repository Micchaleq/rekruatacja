<?php

namespace Polcode\Bundle\RecruitmentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Employee
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min = "3")
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=false,
     *     message="Twoje imie nie może zawierać cyfr."
     * )
     */
    private $firstName;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min = "3")
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=false,
     *     message="Twoje nazwisko nie może zawierać cyfr."
     * )
     */
    private $lastName;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;
    
    /**
     * @Assert\NotBlank()
     */
    private $AM;
    
    /**
     * @Assert\NotBlank()
     */
    private $projects;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function setDefaultValue(LifecycleEventArgs $event)  
    {  
        $entityManager = $event->getEntityManager();
        $project    = $entityManager->getRepository('PolcodeRecruitmentBundle:Project')->findOneByIsInternal(1);
        if($project instanceof \Polcode\Bundle\RecruitmentBundle\Entity\Project && $project){
            $this->addProject($project);
        }
        return $this;
        
    }  

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Employee
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Employee
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Employee
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->projects = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set AM
     *
     * @param \Polcode\Bundle\RecruitmentBundle\Entity\AM $aM
     * @return Employee
     */
    public function setAM(\Polcode\Bundle\RecruitmentBundle\Entity\AM $aM = null)
    {
        $this->AM = $aM;

        return $this;
    }

    /**
     * Get AM
     *
     * @return \Polcode\Bundle\RecruitmentBundle\Entity\AM 
     */
    public function getAM()
    {
        return $this->AM;
    }

    /**
     * Add projects
     *
     * @param \Polcode\Bundle\RecruitmentBundle\Entity\Project $projects
     * @return Employee
     */
    public function addProject(\Polcode\Bundle\RecruitmentBundle\Entity\Project $projects)
    {
        $this->projects[] = $projects;

        return $this;
    }

    /**
     * Remove projects
     *
     * @param \Polcode\Bundle\RecruitmentBundle\Entity\Project $projects
     */
    public function removeProject(\Polcode\Bundle\RecruitmentBundle\Entity\Project $projects)
    {
        $this->projects->removeElement($projects);
    }

    /**
     * Get projects
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProjects()
    {
        return $this->projects;
    }
    public function __toString() {
        return $this->firstName;
    }
}
