<?php

namespace Polcode\Bundle\RecruitmentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Project
 */
class Project
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $endAt;

    /**
     * @var boolean
     * @Assert\NotNull()
     */
    private $isInternal;
    
    private $employees;
    
    /**
     * @Assert\NotBlank()
     */
    private $AM;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Project
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Project
     */
    public function setCreatedAt($createdAt = null)
    {
        if($createdAt)
        {
            $this->createdAt = $createdAt;
        } else
        {
            $this->createdAt = new \DateTime();
        }        

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set endAt
     *
     * @param \DateTime $endAt
     * @return Project
     */
    public function setEndAt($endAt)
    {
        $this->endAt = $endAt;

        return $this;
    }
    
    /**
     * @Assert\True(message = "Data zakończenia musi być większa niż data rozpoczęcia.")
     */
    public function isEndAtVaild(){
        return $this->createdAt >= $this->endAt ? false : true;
    }

    /**
     * Get endAt
     *
     * @return \DateTime 
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * Set isInternal
     *
     * @param boolean $isInternal
     * @return Project
     */
    public function setIsInternal($isInternal)
    {
        $this->isInternal = $isInternal;

        return $this;
    }

    /**
     * Get isInternal
     *
     * @return boolean 
     */
    public function getIsInternal()
    {
        return $this->isInternal;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->employees = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set AM
     *
     * @param \Polcode\Bundle\RecruitmentBundle\Entity\AM $aM
     * @return Project
     */
    public function setAM(\Polcode\Bundle\RecruitmentBundle\Entity\AM $aM = null)
    {
        $this->AM = $aM;

        return $this;
    }

    /**
     * Get AM
     *
     * @return \Polcode\Bundle\RecruitmentBundle\Entity\AM 
     */
    public function getAM()
    {
        return $this->AM;
    }

    /**
     * Add employees
     *
     * @param \Polcode\Bundle\RecruitmentBundle\Entity\Employee $employees
     * @return Project
     */
    public function addEmployee(\Polcode\Bundle\RecruitmentBundle\Entity\Employee $employees)
    {
        $this->employees[] = $employees;

        return $this;
    }

    /**
     * Remove employees
     *
     * @param \Polcode\Bundle\RecruitmentBundle\Entity\Employee $employees
     */
    public function removeEmployee(\Polcode\Bundle\RecruitmentBundle\Entity\Employee $employees)
    {
        $this->employees->removeElement($employees);
    }

    /**
     * Get employees
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEmployees()
    {
        return $this->employees;
    }
    
    public function __toString() {
        return $this->name;
    }
}
